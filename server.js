const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

mongoose
  .connect(
    "mongodb+srv://jroda:admin@zuittbatch243-roda.4axrb9t.mongodb.net/courseBookingAPI?retryWrites=true&w=majority"
  )
  .then(() => {
    app.listen(
      process.env.PORT || 3000,
      console.log("Now connected to MongoDB Atlas"),
      console.log(`Server is running at port ${process.env.PORT || 3000}`)
    );
  })
  .catch((err) => {
    console.log(err);
  });
