const express = require("express");
const router = express.Router();

const { addCourse } = require("../controllers/courseController");
const { verify } = require("../auth");

router.post("/add", addCourse);

module.exports = router;
