const Courses = require("../models/Courses");
const Users = require("../models/Users");
const { decode } = require("../auth");

const addCourse = async (req, res) => {
  const { name, description, price, slots } = req.body;

  let newCourse = new Courses({
    name,
    description,
    price,
    slots,
  });

  const userData = decode(req.headers.authorization);

  try {
    if (userData.isAdmin) {
      await newCourse.save();
      return res.send(`Course added successfully`);
    }

    return res.send(`You cannot add courses, Only admin can add course`);
  } catch (error) {
    console.log(error.message);
    res.send(false);
  }
};

module.exports = {
  addCourse,
};
